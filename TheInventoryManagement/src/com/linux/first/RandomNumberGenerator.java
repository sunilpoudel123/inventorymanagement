package com.linux.first;

import java.util.Random;

public class RandomNumberGenerator {
	static int number=0;
	public RandomNumberGenerator() {
		// TODO Auto-generated constructor stub
		Random random= new Random();
		number= random.nextInt(1000);
		System.out.println("Random number: "+ number);

	}
	public int getRandomNumber() {
		return number;
	}
	public static void main(String[] args) {
		
		RandomNumberGenerator ranNum= new RandomNumberGenerator();
		ranNum.getRandomNumber();

	}

}
