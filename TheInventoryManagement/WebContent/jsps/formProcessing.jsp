<%@page import="com.linux.first.RandomNumberGenerator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div align="center">
<h2>Your Number is processed</h2>
<h4>The random results are:</h4>

<table style="strip">
<tr>
<td> <h2>Your Random number is:  <%= new RandomNumberGenerator().getRandomNumber() %> </h2></td>
</tr>
</table>

</div>
</body>
</html>